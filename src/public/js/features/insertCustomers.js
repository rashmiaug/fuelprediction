import { displayMessage } from "../utilities/message.js";
import { validatedInput } from "./validation.js";
import { loader, removeLoader } from "../utilities/loader.js";
import * as CONFIG from "../config.js";
import { _id } from "../utilities/helper.js";

const submitBtns = _id("sendRequestNewCustomersButton");
const body = document.querySelector('body');
export const insertCustomers = async function (form) {
  try {
    loader(body);
    const res = await fetch(`/request-customer`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Authorization": `Bearer ${ localStorage.getItem( 'token' ) }`
      },
      body: JSON.stringify(form),
    });
    if (!res.ok) throw new Error(`${res.statusText} (${res.status})`);
    const data = await res.json();
    // console.log( 'data:', data );
    if ( data ) {
      // removeLoader();
      displayMessage(`Submitted customer request...`, "success", 3, 'request-customer');
      location.assign('/customers');
    }
  } catch (err) {
    removeLoader();
    displayMessage(`${err}`, "error", 3, 'settings');
  }
};
validatedInput();
if (submitBtns) {
  if (submitBtns.disabled === false) {
    submitBtns.addEventListener("click", (e) => {
      e.preventDefault();

      const first_name = _id("customerInfo_firstName");
      const last_name = _id("customerInfo_lastName");
      const company = _id("customerInfo_companyName");
      const email = _id("customerInfo_email");
      const phone = _id("customerInfo_phoneNumber");
      const address1 = _id("customerInfo_address1");
      const address2 = _id("customerInfo_address2");
      const city = _id("customerInfo_city");
      const state = _id("customerInfo_state");
      const zip_code = _id("customerInfo_zipcode");
      const country = _id("customerInfo_country");

      
      const form = {
        first_name: first_name.value,
        last_name: last_name.value,
        company: company.value,
        email: email.value,
        phone: phone.value,
        address1: address1.value,
        address2: address2.value,
        city: city.value,
        state: state.value,
        zip_code: zip_code.value,
        country: country.value
      };
      insertCustomers(form);
    });
  }
}